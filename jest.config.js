module.exports = {
  projects: [
    '<rootDir>/apps/code-visualiser',
    '<rootDir>/libs/d3',
    '<rootDir>/libs/code-analysis',
    '<rootDir>/libs/http-common',
    '<rootDir>/libs/api',
    '<rootDir>/libs/mylibrary',
    '<rootDir>/libs/app-load',
  ],
};
