import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeSmellEvolutionComponent } from './code-smell-evolution.component';

describe('CodeSmellEvolutionComponent', () => {
  let component: CodeSmellEvolutionComponent;
  let fixture: ComponentFixture<CodeSmellEvolutionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeSmellEvolutionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeSmellEvolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
