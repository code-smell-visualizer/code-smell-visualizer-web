import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Commit} from '@code-visualiser-web/api';

@Component({
  selector: 'code-visualiser-web-code-smell-evolution',
  templateUrl: './code-smell-evolution.component.html',
  styleUrls: ['./code-smell-evolution.component.scss']
})
export class CodeSmellEvolutionComponent implements OnInit, OnChanges {

  @Input() commits: Array<Commit>;
  @Output() commitChangedEventEmitter = new EventEmitter<Commit>();
  selectedCommit: Commit;

  ngOnInit(): void {
    if (this.commits?.length != 0) {
      if (!this.selectedCommit) {
        this.selectedCommit = this.commits[0];
      }
    }
  }

  ngOnChanges(): void {
    if (this.commits?.length != 0) {
      this.selectedCommit = this.commits[0];
    }
  }

  getIconColor(commitId: number): string {
    if (commitId === this.selectedCommit?.id) {
      return 'icon-green';
    }
  }

  getTextColor(commitId: number): string {
    if (commitId === this.selectedCommit?.id) {
      return 'text-success';
    }
  }

  OnCommitChange(commit: Commit): void {
    if (commit.id !== this.selectedCommit.id) {
      this.selectedCommit = commit;
      this.commitChangedEventEmitter.emit(commit);
    }
  }
}
