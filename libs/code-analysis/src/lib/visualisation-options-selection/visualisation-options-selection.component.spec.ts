import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualisationOptionsSelectionComponent } from './visualisation-options-selection.component';

describe('VisualisationOptionsSelectionComponent', () => {
  let component: VisualisationOptionsSelectionComponent;
  let fixture: ComponentFixture<VisualisationOptionsSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualisationOptionsSelectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualisationOptionsSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
