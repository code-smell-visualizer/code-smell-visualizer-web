import {Component, OnInit, Output, EventEmitter, Input, OnChanges} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, startWith, takeUntil} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'code-visualiser-web-visualisation-options-selection',
  templateUrl: './visualisation-options-selection.component.html',
  styleUrls: ['./visualisation-options-selection.component.scss']
})
export class VisualisationOptionsSelectionComponent implements OnInit, OnChanges {
  @Output() colorRangeChangeEvent = new EventEmitter<Array<number>>()
  @Input() nrOfModifiedParameters = 0;
  @Input() branches: Array<string> = [];
  @Output() branchChanged = new EventEmitter<string>();

  value = [1, 3];
  parameterFieldUpdater$ = new Subject<Array<number>>();
  destroy$ = new Subject();

  branchSelectionControl = new FormControl();
  filteredOptions: Observable<string[]>;

  ngOnInit(): void {
    this.initFilterOptions();
    this.parameterFieldUpdater$.pipe(
      takeUntil(this.destroy$),
      debounceTime(800),
      distinctUntilChanged()
    ).subscribe(value => {
      this.colorRangeChangeEvent.emit(value);
    })
  }

  ngOnChanges() {
    this.initFilterOptions();
  }

  private _filter(value: string): string[] {
    const filterValue = value?.toLowerCase();

    return this.branches.filter(option => option?.toLowerCase().includes(filterValue));
  }

  change(event) {
    const newValue: Array<number> = event.newValue;
    this.parameterFieldUpdater$.next(newValue)
  }

  getParametersUrl(): string {
    return '/parameters';
  }

  private initFilterOptions() {
    this.filteredOptions = this.branchSelectionControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  onBranchChange(event) {
    const branch: string = event.option.value;
    this.branchChanged.emit(branch);
  }
}
