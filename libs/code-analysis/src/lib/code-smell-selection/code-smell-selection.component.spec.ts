import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeSmellSelectionComponent } from './code-smell-selection.component';

describe('CodeSmellSelectionComponent', () => {
  let component: CodeSmellSelectionComponent;
  let fixture: ComponentFixture<CodeSmellSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeSmellSelectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeSmellSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
