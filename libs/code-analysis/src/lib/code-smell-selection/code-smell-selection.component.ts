import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { CodeSmell, CodeSmellParameter, CodeSmellImpact } from '@code-visualiser-web/api';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
@Component({
  selector: 'code-visualiser-web-code-smell-selection',
  templateUrl: './code-smell-selection.component.html',
  styleUrls: ['./code-smell-selection.component.scss']
})
export class CodeSmellSelectionComponent implements OnInit {

  @Input() smells: Array<CodeSmell>;
  @Input() title: string;
  @Input() description: string;
  @Input() colorClass = 'card-header-warning';
  @Input() iconColorClass = 'btn-warning';
  @Input() isParamsAdjustable = false;
  @Input() codeSmellImpact: Array<CodeSmellImpact>;
  @Input() colorRange: Array<number> = [5,20];

  @Output() checkBoxEvent = new EventEmitter<CodeSmell>();
  @Output() parameterChangedEvent = new EventEmitter<CodeSmell>();

  parameterFieldUpdater$ = new Subject<string>();
  destroy$ = new Subject();
  codeSmellUnderChange: CodeSmell;
  codeSmellParameterUnderChange: CodeSmellParameter;

  ngOnInit() {
    this.parameterFieldUpdater$
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(500),
        distinctUntilChanged()
      ).subscribe(value => {
        this.changeParameterValue(value);
    })
  }

  width(): string {
    if (this.isParamsAdjustable) {
      return 'half-width';
    } else {
      return 'max-width';
    }
  }

  changeValue(smell: CodeSmell) {
    this.checkBoxEvent.emit(smell);
  }

  onParamValueChange(smell: CodeSmell, parameter: CodeSmellParameter, $event): void {
    this.codeSmellUnderChange = smell;
    this.codeSmellParameterUnderChange = parameter;
    this.parameterFieldUpdater$.next($event);
  }

  changeParameterValue(value: string): void {
    if (!isNaN(+value)) {
      const updatedSmell: CodeSmell = {
        id: this.codeSmellUnderChange.id,
        name: this.codeSmellUnderChange.name,
        description: this.codeSmellUnderChange.description,
        type: this.codeSmellUnderChange.type,
        selected: this.codeSmellUnderChange.selected,
        parameters: this.replaceParameter(this.codeSmellUnderChange.parameters, this.codeSmellParameterUnderChange, +value)
      }
      this.parameterChangedEvent.emit(updatedSmell);
    }
  }

  replaceParameter(parameters: Array<CodeSmellParameter>, parameter: CodeSmellParameter, value: number): Array<CodeSmellParameter> {
    const result: Array<CodeSmellParameter> = []
    for (const param of parameters) {
      if (param.name === parameter.name) {
        result.push({
          id: parameter.id,
          name: parameter.name,
          value: value
        });
      } else {
        result.push(param);
      }
    }
    return result;
  }

  getIconColor(name: string): string {
    const smellImpact: CodeSmellImpact = this.codeSmellImpact.find(smell => smell.name === name);
    if (smellImpact) {
      const adjustedCodeSmellImpact = smellImpact.impact / 3;
      const greenRange = this.colorRange[0];
      const redRange = this.colorRange[1];
      if (adjustedCodeSmellImpact <= greenRange) {
        return 'icon-green'
      } else if (adjustedCodeSmellImpact > greenRange && adjustedCodeSmellImpact <= redRange) {
        return 'icon-yellow'
      } else {
        return 'icon-red'
      }
    }
  }
}

