import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodeSmellSelectionComponent } from './code-smell-selection/code-smell-selection.component';
import {StoreModule} from '@ngrx/store';
import {CODE_ANALYSIS_FEATURE_KEY, analysisReducer} from './+state/analysis.reducer';
import {AnalysisFacade} from './+state/analysis.facade';
import {EffectsModule} from '@ngrx/effects';
import {AnalysisEffects} from './+state/analysis.effects';
import {ApiModule} from '@code-visualiser-web/api';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { VisualisationOptionsSelectionComponent } from './visualisation-options-selection/visualisation-options-selection.component';
import {NgxBootstrapSliderModule} from 'ngx-bootstrap-slider';
import { CodeAnalysisComponent } from './code-analysis/code-analysis.component';
import { CodeAnalysisAdminComponent } from './code-analysis-admin/code-analysis-admin.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import { CodeSmellEvolutionComponent } from './code-smell-evolution/code-smell-evolution.component';

@NgModule({
  imports: [
    ApiModule,
    CommonModule,
    StoreModule.forFeature(CODE_ANALYSIS_FEATURE_KEY, analysisReducer),
    EffectsModule.forFeature([AnalysisEffects]),
    NgSelectModule,
    FormsModule,
    NgxBootstrapSliderModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [CodeAnalysisComponent, CodeAnalysisAdminComponent],
  declarations: [
    CodeSmellSelectionComponent,
    VisualisationOptionsSelectionComponent,
    CodeAnalysisComponent,
    CodeAnalysisAdminComponent,
    CodeSmellEvolutionComponent],
  providers: [AnalysisFacade]
})
export class CodeAnalysisModule {}
