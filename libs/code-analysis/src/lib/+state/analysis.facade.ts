import {Injectable} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {CodeAnalysisPartialState} from './analysis.reducer';
import {
  FetchBranches,
  FetchCodeAnalysis,
  FetchCodeSmells,
  FetchCodeSmellsImpact,
  FetchColorRange, FetchCommits,
  FetchNrOfModifiedParams,
  ResetCodeSmellsUpdated, UpdateAppName, UpdateBranch,
  UpdateCodeSmells
} from './analysis.actions';
import {
  fetchAppName,
  fetchBranch,
  fetchBranches,
  fetchCodeAnalysis,
  fetchCodeSmells,
  fetchCodeSmellsImpact,
  fetchCodeSmellsUpdated,
  fetchColorRange, fetchCommits,
  fetchNrOfModifiedParams,
  isLoading
} from './analysis.selectors';
import {CodeAnalysisInputModel, CodeSmellImpactInputModel, FetchCommitsInputModel} from '@code-visualiser-web/api';

@Injectable({
  providedIn: 'root'
})
export class AnalysisFacade {

  codeAnalysis$ = this.store.pipe(select(fetchCodeAnalysis));
  colorRange$ = this.store.pipe(select(fetchColorRange));
  isLoading$ = this.store.pipe(select(isLoading));
  codeSmells$ = this.store.pipe(select(fetchCodeSmells));
  codeSmellsUpdated$ = this.store.pipe(select(fetchCodeSmellsUpdated));
  nrOfModifiedParams$ = this.store.pipe(select(fetchNrOfModifiedParams));
  codeSmellImpact$ = this.store.pipe(select(fetchCodeSmellsImpact));
  branches$ = this.store.pipe(select(fetchBranches));
  branch$ = this.store.pipe(select(fetchBranch));
  commits$ = this.store.pipe(select(fetchCommits));
  appName$ = this.store.pipe(select(fetchAppName));

  constructor(private store: Store<CodeAnalysisPartialState>) {}

  fetchCodeAnalysis(codeSmells: CodeAnalysisInputModel) {
    this.store.dispatch(new FetchCodeAnalysis(codeSmells));
  }

  fetchColorRange(colorRange: Array<number>) {
    this.store.dispatch(new FetchColorRange(colorRange));
  }

  fetchCodeSmells() {
    this.store.dispatch(new FetchCodeSmells());
  }

  updateCodeSmells(codeSmells: CodeAnalysisInputModel) {
    this.store.dispatch(new UpdateCodeSmells(codeSmells));
  }

  resetCodeSmellsUpdated() {
    this.store.dispatch(new ResetCodeSmellsUpdated());
  }

  fetchNrOfModifiedParameters() {
    this.store.dispatch(new FetchNrOfModifiedParams());
  }

  fetchCodeSmellsImpact(codeSmells: CodeSmellImpactInputModel) {
    this.store.dispatch(new FetchCodeSmellsImpact(codeSmells));
  }

  fetchBranches() {
    this.store.dispatch(new FetchBranches());
  }

  updateBranch(branch: string) {
    this.store.dispatch(new UpdateBranch(branch));
  }

  fetchCommits(branch: string) {
    const input: FetchCommitsInputModel = {
      branch: branch
    }
    this.store.dispatch(new FetchCommits(input));
  }

  updateAppName(appName: string) {
    this.store.dispatch(new UpdateAppName(appName));
  }
}
