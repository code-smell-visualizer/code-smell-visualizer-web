import {createFeatureSelector, createSelector} from '@ngrx/store';
import {CODE_ANALYSIS_FEATURE_KEY, CodeAnalysisState} from './analysis.reducer';

export const getCodeAnalysisState = createFeatureSelector<CodeAnalysisState>(CODE_ANALYSIS_FEATURE_KEY);

export const fetchCodeAnalysis = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.codeAnalysis
);

export const fetchColorRange = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.colorRange
);

export const isLoading = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.isLoading
);

export const fetchCodeSmells = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.codeSmells
)

export const fetchCodeSmellsUpdated = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.codeSmellsUpdated
)

export const fetchNrOfModifiedParams = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.nrOfModifiedParams
)

export const fetchCodeSmellsImpact = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.codeSmellsImpact
)

export const fetchBranches = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.branches
)

export const fetchBranch = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.branch
)

export const fetchCommits = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.commits
)

export const fetchAppName = createSelector(
  getCodeAnalysisState,
  (state: CodeAnalysisState) => state.appName
)
