import { Action } from '@ngrx/store';
import {
  CodeAnalysisInputModel,
  CodeAnalysisResponse,
  CodeSmell, CodeSmellImpact,
  CodeSmellImpactInputModel, Commit, FetchCommitsInputModel
} from '@code-visualiser-web/api';

export enum CodeAnalysisActionTypes {
  FetchCodeAnalysis = '[CodeAnalysis] Fetch Code Analysis',
  FetchCodeAnalysisSuccess = '[CodeAnalysis] Fetch Code Analysis Success',
  FetchColorRange = '[CodeAnalysis] Fetch Color Range',
  FetchCodeSmells = '[CodeSmells] Fetch Code Smells',
  FetchCodeSmellsSuccess = '[CodeSmells] Fetch Code Smells Success',
  UpdateCodeSmells = '[CodeSmells] Update Code Smells',
  UpdateCodeSmellsSuccess = '[CodeSmells] Update Code Smells Success',
  ResetCodeSmellsUpdated = '[CodeSmells] Reset Code Smells Updated',
  FetchNrOfModifiedParams = '[CodeSmells] Fetch Nr Of Modified Params',
  FetchNrOfModifiedParamsSuccess = '[CodeSmells] Fetch Nr Of Modified Params Success',
  FetchCodeSmellImpact = '[CodeSmells] Fetch Code Smell Impact',
  FetchCodeSmellImpactSuccess = '[CodeSmells] Fetch Code Smell Impact Success',
  FetchBranches = '[CodeAnalysis] Fetch Branches',
  FetchBranchesSuccess = '[CodeAnalysis] Fetch Branches Success',
  UpdateBranch = '[CodeAnalysis] Update Branch',
  FetchCommits = '[CodeEvolution] Fetch Commits',
  FetchCommitsSuccess = '[CodeEvolution] Fetch Commits Success',
  UpdateAppName = '[CodeAnalysis] Update App Name',
}

export class FetchCodeAnalysis implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeAnalysis;

  constructor(
    public codeSmells: CodeAnalysisInputModel) {
  }
}

export class FetchCodeAnalysisSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeAnalysisSuccess;

  constructor(
    public queryResult: CodeAnalysisResponse,
  ) {
  }
}

export class FetchColorRange implements Action {
  readonly type = CodeAnalysisActionTypes.FetchColorRange;

  constructor(
    public colorRange: Array<number>,
  ) {
  }
}

export class FetchCodeSmells implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeSmells;
}

export class FetchCodeSmellsSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeSmellsSuccess;

  constructor(
    public queryResult: Array<CodeSmell>,
  ) {
  }
}

export class UpdateCodeSmells implements Action {
  readonly type = CodeAnalysisActionTypes.UpdateCodeSmells;

  constructor(
    public codeSmells: CodeAnalysisInputModel,
  ) {
  }
}

export class UpdateCodeSmellsSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.UpdateCodeSmellsSuccess;
}

export class ResetCodeSmellsUpdated implements Action {
  readonly type = CodeAnalysisActionTypes.ResetCodeSmellsUpdated;
}

export class FetchNrOfModifiedParams implements Action {
  readonly type = CodeAnalysisActionTypes.FetchNrOfModifiedParams;
}

export class FetchNrOfModifiedParamsSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchNrOfModifiedParamsSuccess;

  constructor(
    public nrOfModifiedParams: number,
  ) {
  }
}

export class FetchCodeSmellsImpact implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeSmellImpact;

  constructor(
    public codeSmells: CodeSmellImpactInputModel) {
  }
}

export class FetchCodeSmellsImpactSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCodeSmellImpactSuccess;

  constructor(
    public queryResult: Array<CodeSmellImpact>,
  ) {
  }
}

export class FetchBranches implements Action {
  readonly type = CodeAnalysisActionTypes.FetchBranches;
}

export class FetchBranchesSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchBranchesSuccess;
  constructor(
    public branches: Array<string>,
  ) {
  }
}

export class UpdateBranch implements Action {
  readonly type = CodeAnalysisActionTypes.UpdateBranch;
  constructor(
    public branch: string,
  ) {
  }
}

export class FetchCommits implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCommits;
  constructor(
    public branch: FetchCommitsInputModel,
  ) {
  }
}

export class FetchCommitsSuccess implements Action {
  readonly type = CodeAnalysisActionTypes.FetchCommitsSuccess;
  constructor(
    public commits: Array<Commit>,
  ) {
  }
}

export class UpdateAppName implements Action {
  readonly type = CodeAnalysisActionTypes.UpdateAppName;
  constructor(
    public appName: string,
  ) {
  }
}

export type ClassesAction =
  | FetchCodeAnalysis
  | FetchCodeAnalysisSuccess
  | FetchColorRange
  | FetchCodeSmells
  | FetchCodeSmellsSuccess
  | UpdateCodeSmells
  | UpdateCodeSmellsSuccess
  | ResetCodeSmellsUpdated
  | FetchNrOfModifiedParams
  | FetchNrOfModifiedParamsSuccess
  | FetchCodeSmellsImpact
  | FetchCodeSmellsImpactSuccess
  | FetchBranches
  | FetchBranchesSuccess
  | UpdateBranch
  | FetchCommits
  | FetchCommitsSuccess
  | UpdateAppName
