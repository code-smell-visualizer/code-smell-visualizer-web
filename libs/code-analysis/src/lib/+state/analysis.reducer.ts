import {ClassAnalysis, CodeSmell, CodeSmellImpact, Commit} from '@code-visualiser-web/api';
import {ClassesAction, CodeAnalysisActionTypes} from './analysis.actions';

export const CODE_ANALYSIS_FEATURE_KEY = 'analysis'

export interface CodeAnalysisState {
  codeAnalysis: Array<ClassAnalysis>;
  colorRange: Array<number>;
  isLoading: boolean;
  codeSmellsUpdated: boolean;
  codeSmells: Array<CodeSmell>
  nrOfModifiedParams: number;
  codeSmellsImpact: Array<CodeSmellImpact>;
  branches: Array<string>;
  branch: string;
  commits: Array<Commit>;
  appName: string;
}

export interface CodeAnalysisPartialState {
  readonly [CODE_ANALYSIS_FEATURE_KEY]: CodeAnalysisState
}

export const initialState: CodeAnalysisState = {
  codeAnalysis: [],
  colorRange: [5, 20],
  isLoading: false,
  codeSmellsUpdated: false,
  codeSmells: [],
  nrOfModifiedParams: 0,
  codeSmellsImpact: [],
  branches: [],
  branch: null,
  commits: [],
  appName: null,
}

export function analysisReducer(
  state: CodeAnalysisState = initialState,
  action: ClassesAction
) {
  switch (action.type) {
    case CodeAnalysisActionTypes.FetchCodeAnalysis:
    case CodeAnalysisActionTypes.FetchCodeSmells:
    case CodeAnalysisActionTypes.UpdateCodeSmells:
    case CodeAnalysisActionTypes.FetchNrOfModifiedParams:
    case CodeAnalysisActionTypes.FetchCodeSmellImpact:
    case CodeAnalysisActionTypes.FetchBranches:
    case CodeAnalysisActionTypes.FetchCommits:
      return {
        ...state,
        isLoading: true
      };
    case CodeAnalysisActionTypes.FetchCodeAnalysisSuccess:
      return {
        ...state,
        codeAnalysis: action.queryResult.classes,
        isLoading: false
      };
    case CodeAnalysisActionTypes.FetchColorRange:
      return {
        ...state,
        colorRange: action.colorRange
      };
    case CodeAnalysisActionTypes.FetchCodeSmellsSuccess:
      return {
        ...state,
        isLoading: true,
        codeSmells: action.queryResult
      };
    case CodeAnalysisActionTypes.UpdateCodeSmellsSuccess:
      return {
        ...state,
        isLoading: false,
        codeSmellsUpdated: true,
      };
    case CodeAnalysisActionTypes.ResetCodeSmellsUpdated:
      return {
        ...state,
        codeSmellsUpdated: false,
      };
    case CodeAnalysisActionTypes.FetchNrOfModifiedParamsSuccess:
      return {
        ...state,
        isLoading: false,
        nrOfModifiedParams: action.nrOfModifiedParams
      };
    case CodeAnalysisActionTypes.FetchCodeSmellImpactSuccess:
      return {
        ...state,
        isLoading: false,
        codeSmellsImpact: action.queryResult
      };
    case CodeAnalysisActionTypes.FetchBranchesSuccess:
      return {
        ...state,
        isLoading: false,
        branches: action.branches
      };
    case CodeAnalysisActionTypes.UpdateBranch:
      return {
        ...state,
        isLoading: false,
        branch: action.branch
      };
    case CodeAnalysisActionTypes.FetchCommitsSuccess:
      return {
        ...state,
        isLoading: false,
        commits: action.commits
      };
    case CodeAnalysisActionTypes.UpdateAppName:
      return {
        ...state,
        isLoading: false,
        appName: action.appName
      };
    default:
      return state;
  }

}
