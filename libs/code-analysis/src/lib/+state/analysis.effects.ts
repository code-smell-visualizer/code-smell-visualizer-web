import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataPersistence} from '@nrwl/angular';
import {CodeAnalysisPartialState} from './analysis.reducer';
import {
  CodeAnalysisActionTypes,
  FetchBranchesSuccess,
  FetchCodeAnalysis,
  FetchCodeAnalysisSuccess,
  FetchCodeSmellsImpact,
  FetchCodeSmellsImpactSuccess,
  FetchCodeSmellsSuccess,
  FetchCommits,
  FetchCommitsSuccess,
  FetchNrOfModifiedParamsSuccess,
  UpdateCodeSmells,
  UpdateCodeSmellsSuccess
} from './analysis.actions';
import { Effect } from '@ngrx/effects';
import {map} from 'rxjs/operators';
import {CodeAnalysisResponse, CodeSmell, CodeSmellImpact, Commit} from '@code-visualiser-web/api';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import {HttpCommon} from '../../../../http-common';
import {Router} from '@angular/router';

@Injectable()
export class AnalysisEffects {

  private baseUrl = '/proxy/code-visualiser/api'

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<CodeAnalysisPartialState>
  ) {}

  @Effect() fetchCodeAnalysis$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchCodeAnalysis,
    {
      run: (action: FetchCodeAnalysis) => {
        return this.httpClient
          .post(
            this.baseUrl + '/code-analysis',
            JSON.stringify(action.codeSmells),
            { headers: HttpCommon.headers }
          )
          .pipe(
            map(resp => {
              return new FetchCodeAnalysisSuccess(
                resp as CodeAnalysisResponse
              );
            })
          );
      }
    }
  )

  @Effect() fetchCodeSmells$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchCodeSmells,
    {
      run: () => {
        return this.httpClient
          .get(this.baseUrl + '/code-smell')
          .pipe(
            map(resp => {
              return new FetchCodeSmellsSuccess(
                resp as Array<CodeSmell>,
              );
            })
          );
      }
    }
  )

  @Effect() updateCodeSmells$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.UpdateCodeSmells,
    {
      run: (action: UpdateCodeSmells) => {
        return this.httpClient
          .post(
            this.baseUrl + '/code-smell',
            JSON.stringify(action.codeSmells),
            { headers: HttpCommon.headers }
          )
          .pipe(
            map(() => {
              this.router.navigate(['']).then(() => false);
              return new UpdateCodeSmellsSuccess();
            })
          );
      }
    }
  )

  @Effect() fetchNrOfModifiedParams$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchNrOfModifiedParams,
    {
      run: () => {
        return this.httpClient
          .get(this.baseUrl + '/code-smell/nr-of-params')
          .pipe(
            map(resp => {
              return new FetchNrOfModifiedParamsSuccess(
                resp as number,
              );
            })
          );
      }
    }
  )

  @Effect() fetchCodeSmellImpact$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchCodeSmellImpact,
    {
      run: (action: FetchCodeSmellsImpact) => {
        return this.httpClient
          .post(
            this.baseUrl + '/code-smell-impact',
            JSON.stringify(action.codeSmells),
            { headers: HttpCommon.headers }
          )
          .pipe(
            map(resp => {
              return new FetchCodeSmellsImpactSuccess(
                resp as Array<CodeSmellImpact>
              );
            })
          );
      }
    }
  )

  @Effect() fetchBranches$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchBranches,
    {
      run: () => {
        return this.httpClient
          .get(this.baseUrl + '/code-evolution/branches')
          .pipe(
            map(resp => {
              return new FetchBranchesSuccess(
                resp as Array<string>,
              );
            })
          );
      }
    }
  )

  @Effect() fetchCommits$ = this.dataPersistence.fetch(
    CodeAnalysisActionTypes.FetchCommits,
    {
      run: (action: FetchCommits) => {
        return this.httpClient
          .post(
            this.baseUrl + '/code-evolution/commits',
            JSON.stringify(action.branch),
            { headers: HttpCommon.headers }
          )
          .pipe(
            map(resp => {
              return new FetchCommitsSuccess(
                resp as Array<Commit>,
              );
            })
          );
      }
    }
  )

}
