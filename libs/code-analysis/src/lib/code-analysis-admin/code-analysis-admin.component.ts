import { Component, OnInit } from '@angular/core';
import {CodeAnalysisInputModel, CodeSmell, CodeSmellType} from '@code-visualiser-web/api';
import {AnalysisFacade} from '@code-visualiser-web/code-analysis';

@Component({
  selector: 'code-visualiser-web-code-analysis-admin',
  templateUrl: './code-analysis-admin.component.html',
  styleUrls: ['./code-analysis-admin.component.scss']
})
export class CodeAnalysisAdminComponent implements OnInit {

  smells: Array<CodeSmell>;
  classSmells: Array<CodeSmell>;
  methodSmells: Array<CodeSmell>;
  appName: string;

  constructor(
    private analysisFascade: AnalysisFacade,
  ) {}

  ngOnInit(): void {
    this.appName = localStorage.getItem('appName');
    this.analysisFascade.fetchCodeSmells();
    this.analysisFascade.codeSmells$.pipe().subscribe(smells => {
      if (smells) {
        this.initCodeSmells(smells);
      }
    })
    this.analysisFascade.appName$.pipe().subscribe(name => {
      if (name) {
        this.appName = name;
      }
    })
  }

  initCodeSmells(smells: Array<CodeSmell>): void {
    this.smells = smells;
    this.classSmells = smells.filter(smell => smell.type == CodeSmellType.ClassBased);
    this.methodSmells = smells.filter(smell => smell.type == CodeSmellType.MethodBased);
  }

  codeSmellChanged(codeSmell: CodeSmell): void {
    const result: Array<CodeSmell> = []
    for (const smell of this.smells) {
      if (smell.name === codeSmell.name) {
        result.push({
          id: codeSmell.id,
          name: codeSmell.name,
          description: codeSmell.description,
          parameters: codeSmell.parameters,
          selected: !codeSmell.selected,
          type: codeSmell.type
        });
      } else {
        result.push(smell);
      }
    }
    this.initCodeSmells(result);
  }

  saveParameterValues(): void {
    const codeSmellInput: CodeAnalysisInputModel = {
      codeSmells: this.smells,
      appName: this.appName
    }
    this.analysisFascade.updateCodeSmells(codeSmellInput);
  }
}
