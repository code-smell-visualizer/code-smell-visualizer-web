import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeAnalysisAdminComponent } from './code-analysis-admin.component';

describe('CodeAnalysisAdminComponent', () => {
  let component: CodeAnalysisAdminComponent;
  let fixture: ComponentFixture<CodeAnalysisAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeAnalysisAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeAnalysisAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
