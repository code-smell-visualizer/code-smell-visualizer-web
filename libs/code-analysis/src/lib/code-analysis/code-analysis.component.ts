import { Component, OnInit } from '@angular/core';
import {AnalysisFacade} from '@code-visualiser-web/code-analysis';
import {
  CodeAnalysisInputModel,
  CodeSmell,
  CodeSmellImpactInputModel,
  CodeSmellType,
  Commit
} from '@code-visualiser-web/api';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from "@angular/router";

@Component({
  selector: 'code-visualiser-web-code-analysis',
  templateUrl: './code-analysis.component.html',
  styleUrls: ['./code-analysis.component.scss']
})
export class CodeAnalysisComponent implements OnInit {

  constructor(
    private analysisFacade: AnalysisFacade,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}

  smells: Array<CodeSmell>;
  classSmells: Array<CodeSmell>;
  methodSmells: Array<CodeSmell>;
  colorRange: Array<number> = [5, 20];
  queriedCodeSmells: Array<CodeSmell> = [];

  commit: Commit;
  commitChanged = false;

  branch: string;
  branchChanged = false;

  appName: string

  nrOfParametersModified$ = this.analysisFacade.nrOfModifiedParams$;
  codeSmellImpact$ = this.analysisFacade.codeSmellImpact$;
  branches$ = this.analysisFacade.branches$;
  commits$ = this.analysisFacade.commits$;

  ngOnInit(): void {
    this.appName = localStorage.getItem('appName');
    this.analysisFacade.fetchCodeSmells();
    this.analysisFacade.fetchNrOfModifiedParameters();
    this.analysisFacade.fetchBranches();
    this.analysisFacade.codeSmells$.pipe().subscribe(smells => {
      this.initCodeSmells(smells);
      this.getCodeSmellsImpact(smells);
    });
    this.analysisFacade.codeSmellsUpdated$.pipe().subscribe(updated => {
      if (updated) {
        this.showSnackBar();
        this.analysisFacade.resetCodeSmellsUpdated();
      }
    });
    this.analysisFacade.appName$.pipe().subscribe(name => {
      if (name) {
        this.appName = name;
      }
    })
  }

  initCodeSmells(smells: Array<CodeSmell>): void {
    this.smells = smells
    this.classSmells = smells.filter(smell => smell.type == CodeSmellType.ClassBased);
    this.methodSmells = smells.filter(smell => smell.type == CodeSmellType.MethodBased);
  }

  onRangeChange(range: Array<number>): void {
    this.colorRange = range;
    this.analysisFacade.fetchColorRange(this.colorRange);
  }

  onBranchChange(branch: string): void {
    this.branch = branch;
    this.analysisFacade.updateBranch(branch);
    this.analysisFacade.fetchCommits(this.branch);
    this.branchChanged = true;
    this.loadClasses();
    this.getCodeSmellsImpact(this.smells);
    this.branchChanged = false;
  }

  codeSmellChanged(codeSmell: CodeSmell): void {
    const result: Array<CodeSmell> = []
    for (const smell of this.smells) {
      if (smell.name === codeSmell.name) {
        result.push({
          id: codeSmell.id,
          name: codeSmell.name,
          description: codeSmell.description,
          parameters: codeSmell.parameters,
          selected: !codeSmell.selected,
          type: codeSmell.type
        });
      } else {
        result.push(smell);
      }
    }
    this.initCodeSmells(result);
  }

  loadClasses(): void {
    const codeSmells: Array<CodeSmell> = this.smells
      .map(x => Object.assign({}, x))
      .filter(smell => smell.selected);

    this.analysisFacade.fetchColorRange(this.colorRange);

    if (codeSmells.length > 0 &&
      (this.codeSmellsChanged(codeSmells)
        || this.branchChanged
        || this.commitChanged)) {
      const codeSmellInput: CodeAnalysisInputModel = {
        commitId: this.commit ? this.commit.id : null,
        branch: this.branch ? this.branch : null,
        codeSmells: codeSmells,
        appName: this.appName,
      }
      this.analysisFacade.fetchCodeAnalysis(codeSmellInput);
      this.queriedCodeSmells = codeSmells
    }
  }

  onCommitChanged(commit: Commit) {
    this.commit = commit;
    this.commitChanged = true;
    this.loadClasses();
    this.commitChanged = false;
  }

  private codeSmellsChanged(codeSmells: Array<CodeSmell>): boolean {
    const queriedSmells = this.queriedCodeSmells.map(x => Object.assign({}, x))
    if (codeSmells.length !== queriedSmells.length) {
      return true;
    }
    if (codeSmells.length > 1) {
      codeSmells.sort((a,b) => a.name.localeCompare(b.name));
      queriedSmells.sort((a, b) => a.name.localeCompare(b.name));
    }

    for (let i = 0; i < codeSmells.length; ++i) {
      if (codeSmells[i].name !== queriedSmells[i].name) {
        return true;
      }
    }

    return false;
  }

  private showSnackBar(): void {
    this._snackBar.open('Code Smell parameters updated', 'close', {
      duration: 4000,
      panelClass: ['green-snackbar']
    });
  }

  private getCodeSmellsImpact(smells: Array<CodeSmell>) {
    const codeSmellImpactInput: CodeSmellImpactInputModel = {
      branch: this.branch ? this.branch : null,
      codeSmells: smells,
      appName: this.appName,
    };
    this.analysisFacade.fetchCodeSmellsImpact(codeSmellImpactInput);
  }

  changeApp() {
    this.router.navigate(['/upload']).then(() => false);
  }
}
