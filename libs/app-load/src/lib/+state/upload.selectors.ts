import {createFeatureSelector, createSelector} from '@ngrx/store';
import {UPLOAD_FEATURE_KEY, UploadState} from './upload.reducer';

export const getUploadState = createFeatureSelector<UploadState>(UPLOAD_FEATURE_KEY);

export const fetchExistingApplications = createSelector(
  getUploadState,
  (state: UploadState) => state.existingApplications
);
