import {Action} from '@ngrx/store';
import {UploadAppInputModel} from "@code-visualiser-web/api";

export enum UploadActionsTypes {
  FetchExistingApplications = '[Upload] Fetch Existing Applications',
  FetchExistingApplicationsSuccess = '[Upload] Fetch Existing Applications Success',
  UploadApp = '[Upload] Upload App',
  UploadAppSuccess = '[Upload] Upload App Success',
}

export class FetchExistingApplications implements Action {
  readonly type = UploadActionsTypes.FetchExistingApplications;
}

export class FetchExistingApplicationsSuccess implements Action {
  readonly type = UploadActionsTypes.FetchExistingApplicationsSuccess;

  constructor(
    public existingApplications: Array<string>
  ) {
  }
}

export class UploadApp implements Action {
  readonly type = UploadActionsTypes.UploadApp;

  constructor(
    public appPath: UploadAppInputModel
  ) {
  }
}

export class UploadAppSuccess implements Action {
  readonly type = UploadActionsTypes.UploadAppSuccess;

  constructor(
    public appName: string
  ) {
  }
}


export type UploadAction =
  | FetchExistingApplications
  | FetchExistingApplicationsSuccess
  | UploadApp
  | UploadAppSuccess
