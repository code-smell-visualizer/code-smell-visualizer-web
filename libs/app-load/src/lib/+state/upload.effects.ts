import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {DataPersistence} from '@nrwl/angular';
import {UploadPartialState} from './upload.reducer';
import {Effect} from '@ngrx/effects';
import {FetchExistingApplicationsSuccess, UploadActionsTypes, UploadApp, UploadAppSuccess} from './upload.actions';
import {map} from 'rxjs/operators';
import {HttpCommon} from '../../../../http-common';

@Injectable()
export class UploadEffects {

  private baseUrl = '/proxy/code-visualiser/api';

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<UploadPartialState>
  ) {
  }

  @Effect() fetchExistingApplications$ = this.dataPersistence.fetch(
    UploadActionsTypes.FetchExistingApplications,
    {
      run: () => {
        return this.httpClient
          .get(this.baseUrl + '/upload/existing-applications')
          .pipe(
            map(resp => {
              return new FetchExistingApplicationsSuccess(
                resp as Array<string>,
              );
            })
          );
      }
    }
  )

  @Effect() fetchCodeAnalysis$ = this.dataPersistence.fetch(
    UploadActionsTypes.UploadApp,
    {
      run: (action: UploadApp) => {
        return this.httpClient
          .post(
            this.baseUrl + '/upload',
            JSON.stringify(action.appPath),
            { headers: HttpCommon.headers }
          )
          .pipe(
            map(resp => {
              return new UploadAppSuccess(
                resp as string
              );
            })
          );
      }
    }
  )

}
