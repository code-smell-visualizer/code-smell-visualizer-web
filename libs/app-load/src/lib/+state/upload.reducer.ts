import {UploadAction, UploadActionsTypes} from './upload.actions';

export const UPLOAD_FEATURE_KEY = 'upload';

export interface UploadState {
  existingApplications: Array<string>;
  isLoading: boolean;
}

export interface UploadPartialState {
  readonly [UPLOAD_FEATURE_KEY]: UploadState;
}

export const initialState: UploadState = {
  existingApplications: [],
  isLoading: false
}

export function uploadReducer(
  state: UploadState = initialState,
  action: UploadAction
) {
  switch (action.type) {
    case UploadActionsTypes.FetchExistingApplications:
    case UploadActionsTypes.UploadApp:
      return {
        ...state,
        isLoading: true
      };
    case UploadActionsTypes.FetchExistingApplicationsSuccess:
      return {
        ...state,
        isLoading: false,
        existingApplications: action.existingApplications,
      };
    case UploadActionsTypes.UploadAppSuccess:
      return {
        ...state,
        isLoading: false,
        // TODO 25/03/2021: Maybe assign app name
      };

    default:
      return state;
  }

}
