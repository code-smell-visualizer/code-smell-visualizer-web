import {Injectable} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {UploadPartialState} from './upload.reducer';
import {fetchExistingApplications} from './upload.selectors';
import {FetchExistingApplications, UploadApp} from './upload.actions';
import {UploadAppInputModel} from '@code-visualiser-web/api';

@Injectable({
  providedIn: 'root'
})
export class UploadFacade {

  existingApplications$ = this.store.pipe(select(fetchExistingApplications));

  constructor(private store: Store<UploadPartialState>) {}

  fetchExistingApplications() {
    this.store.dispatch(new FetchExistingApplications());
  }

  uploadApp(selectedPath: string) {
    const uploadAppInputModel: UploadAppInputModel = {
      appPath: selectedPath
    }
    this.store.dispatch(new UploadApp(uploadAppInputModel));
  }
}
