import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload/upload.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {UPLOAD_FEATURE_KEY, uploadReducer} from './+state/upload.reducer';
import {UploadEffects} from './+state/upload.effects';
import {UploadFacade} from './+state/upload.facade';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    StoreModule.forFeature(UPLOAD_FEATURE_KEY, uploadReducer),
    EffectsModule.forFeature([UploadEffects]),
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [UploadComponent],
  exports: [UploadComponent],
  providers: [UploadFacade]
})
export class AppLoadModule {}
