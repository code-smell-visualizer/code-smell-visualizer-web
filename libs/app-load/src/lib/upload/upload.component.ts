import {Component, OnInit} from '@angular/core';
import {UploadFacade} from '../+state/upload.facade';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AnalysisFacade} from '@code-visualiser-web/code-analysis';
import {Router} from '@angular/router';

@Component({
  selector: 'code-visualiser-web-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(
    private router: Router,
    private uploadFacade: UploadFacade,
    private analysisFacade: AnalysisFacade
  ) { }

  existingApplications: Array<string>;
  appSelectionControl = new FormControl();
  filteredOptions: Observable<string[]>;
  selectedApp: string;
  selectedPath: string;

  ngOnInit(): void {
    this.uploadFacade.fetchExistingApplications();
    this.uploadFacade.existingApplications$.pipe().subscribe(applications => {
      if (applications) {
        this.existingApplications = applications;
        this.initFilterOptions();
      }
    })
  }

  onAppSelected(event) {
    this.selectedApp = event.option.value;
  }

  private initFilterOptions() {
    this.filteredOptions = this.appSelectionControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.existingApplications.filter(option => option.toLowerCase().includes(filterValue));
  }

  analyse(): void {
    if (this.selectedPath) {
      //this.analysisFacade.updateAppName(this)
      // TODO 25/03/2021: some heavier logic
      this.uploadFacade.uploadApp(this.selectedPath);
    } else if (this.selectedApp) {
      localStorage.setItem('appName', this.selectedApp);
      this.analysisFacade.updateAppName(this.selectedApp);
      this.router.navigate(['']).then(() => false);

    }
  }

  return(): void {
    console.log(localStorage.getItem('appName'));
    this.router.navigate(['']).then(() => false);
  }

  isCancelButtonHidden(): boolean {
    console.log(localStorage.getItem('appName'));
    return localStorage.getItem('appName') === null;
  }
}
