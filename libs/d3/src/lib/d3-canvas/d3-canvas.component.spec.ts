import { ComponentFixture, TestBed } from '@angular/core/testing';

import { D3CanvasComponent } from './d3-canvas.component';

describe('D3CanvasComponent', () => {
  let component: D3CanvasComponent;
  let fixture: ComponentFixture<D3CanvasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ D3CanvasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(D3CanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
