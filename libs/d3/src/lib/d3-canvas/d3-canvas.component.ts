import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as d3 from 'd3';
import {ClassAnalysis} from '@code-visualiser-web/api';
import {SimulationData} from '../model/simulation-data';

@Component({
  selector: 'code-visualiser-web-d3-canvas',
  templateUrl: './d3-canvas.component.html',
  styleUrls: ['./d3-canvas.component.scss']
})
export class D3CanvasComponent implements OnInit, OnChanges {
  private static minimumZoomLevel = 1;
  private static maximumZoomLevel = 15;
  private static labelSizeRadiusRatio = 5;

  d3:d3.Simulation<never, never>;
  @Input() isLoading: boolean;
  @Input() classesAnalysis: Array<ClassAnalysis>;
  @Input() colorRange: Array<number>;

  private svg;
  private simulation;
  private data: SimulationData[];
  private zoom;
  private width = 1600;
  private height = 900;
  private g;
  private bubbles;
  private labels;

  private static getLabelOpacity(bubbleRadius: number, zoomLevel: number): number {
    const bubbleRelativeRadius = bubbleRadius * zoomLevel;
    if (bubbleRelativeRadius > 50) {
      return 1;
    } else if (bubbleRelativeRadius > 30) {
      return bubbleRelativeRadius / 50;
    } else {
      return 0;
    }
  }

  private static splitLabel(label: string): Array<string> {
    const labels = label.split(/(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])/).filter(n => n != null);
    if (labels.length >= 3) {
      const splitPoint = (Math.floor(labels.length / 2) + (labels.length % 2));
      const first = labels.slice(0, splitPoint).join('');
      const second = labels.slice(splitPoint).join('');
      return [first, second];
    }
    return [label];
  }

  ngOnInit(): void {
    this.initSVG();
    this.initSimulation();
    this.initZoom();
  }

  private initSVG(): void {
    if (this.svg) {
      this.svg.remove();
    }
    this.svg = d3.select('#canvas')
      .append('svg')
      .attr('viewBox', <never>[0,0, this.width, this.height]);
    this.g = this.svg.append('g');
  }

  private initSimulation(): void {
    this.simulation = d3.forceSimulation()
      .force('x', d3.forceX((d: SimulationData) => d.xforce).strength(this.getRandomForce()))
      .force('y', d3.forceY((d: SimulationData) => d.yforce).strength(3*this.getRandomForce()))
      .force('collide', d3.forceCollide()
        .radius((d: SimulationData) => d.radius));
  }

  private getRandomForce() {
    return this.getRandomNuber(0.01, 0.10);
  }

  private getRandomNuber(min: number, max: number) {
    return Math.random() * (max - min) + min
  }

  private initZoom(): void {
    this.zoom = d3.zoom().scaleExtent([D3CanvasComponent.minimumZoomLevel, D3CanvasComponent.maximumZoomLevel])
      .on('zoom', ({transform}) => {
        this.g.attr('transform', transform);
        this.labels.attr('class', null);
        this.labels.attr('opacity', d => D3CanvasComponent.getLabelOpacity(d.radius, transform.k))
      })
  }

  ngOnChanges(): void {
    this.updateData();
    if (!this.isLoading && this.data?.length > 0 && this.svg) {
      this.initSVG();
      this.initSimulation();

      this.initBubbles();
      this.initLabels();

      this.simulation.nodes(this.data)
        .on('tick', () => {
          this.adjustBubblesPlacement();
          this.adjustLabelsPlacement();
        })

      this.svg.call(this.zoom);
    }
  }

  private updateData() {
    if (this.classesAnalysis) {
      this.data = this.classesAnalysis.map(analysis => {
        return {
          labels: D3CanvasComponent.splitLabel(analysis.name),
          radius: analysis.size,
          color: this.extractColor(analysis.score),
          labelSizeScale: analysis.size / D3CanvasComponent.labelSizeRadiusRatio,
          xforce: analysis.force.x,
          yforce: analysis.force.y
        }
      })
    }
  }

  private extractColor(score: number): string {
    if (this.colorRange) {
      const greenRange = this.colorRange[0];
      const redRange = this.colorRange[1];
      if (score <= greenRange) {
        return '#69b3a2';
      } else if (score > greenRange && score <= redRange) {
        return '#dadb81';
      } else {
        return '#b36969';
      }
    }
    return '#69b3a2';
  }

  private initBubbles(): void {
    this.bubbles = this.g.selectAll('circle')
      .data(this.data)
      .enter().append('circle')
      .attr('r', d => d.radius)
      .attr('fill', d => d.color);
  }

  private initLabels(): void {
    this.labels = this.g.selectAll('text')
      .data(this.data)
      .enter()
      .append('text')
      .text((d:SimulationData) => d.labels[0])
      .attr('class', 'invisible');

    this.labels
      .append('tspan')
      .data(this.data)
      .text((d:SimulationData) => d.labels.length > 1 ? d.labels[1] : '');

    this.labels.style('font-size', (d:SimulationData) => (d.labelSizeScale + 'px'));
  }

  private adjustBubblesPlacement(): void {
    this.bubbles
      .attr('cx', (d:SimulationData) => d.x)
      .attr('cy', (d:SimulationData) => d.y);
  }

  private adjustLabelsPlacement(): void {
    this.labels
      .attr('x', (d:SimulationData) => d.x)
      .attr('y', (d:SimulationData) => d.labels.length > 1 ? d.y - d.radius/10 : d.y + 0.5);

    this.labels.selectAll('tspan')
      .attr('x', d => d.x)
      .attr('y', d => d.y + d.radius/8);
  }
}
