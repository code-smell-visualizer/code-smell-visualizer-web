import { SimulationNodeDatum } from 'd3';

export interface SimulationData extends SimulationNodeDatum{
  radius: number;
  labels: Array<string>;
  color: string;
  labelSizeScale: number;
  xforce: number;
  yforce: number;
}
