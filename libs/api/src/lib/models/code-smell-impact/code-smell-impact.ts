export interface CodeSmellImpact {
  name: string;
  impact: number;
}
