import {CodeSmell} from '@code-visualiser-web/api';

export class CodeSmellImpactInputModel {

  branch?: string;
  codeSmells: Array<CodeSmell>;
  appName: string;
}
