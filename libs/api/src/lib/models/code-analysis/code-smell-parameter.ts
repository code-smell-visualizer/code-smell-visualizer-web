export interface CodeSmellParameter {
  id: number;
  name: string;
  value: number;
}
