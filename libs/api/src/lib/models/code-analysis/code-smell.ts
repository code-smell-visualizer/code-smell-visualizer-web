import {CodeSmellType} from './enums/code-smell-type';
import {CodeSmellParameter} from './code-smell-parameter';

export interface CodeSmell {
  id: number;
  name: string;
  parameters?: Array<CodeSmellParameter>;
  selected: boolean;
  type: CodeSmellType;
  description?: string;
}
