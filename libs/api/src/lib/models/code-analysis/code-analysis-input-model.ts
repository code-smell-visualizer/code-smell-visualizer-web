import {CodeSmell} from '@code-visualiser-web/api';

export class CodeAnalysisInputModel {
  commitId?: number;
  branch?: string;
  codeSmells: Array<CodeSmell>;
  appName: string
}
