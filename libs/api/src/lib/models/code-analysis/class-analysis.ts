import {Force} from './force';

export interface ClassAnalysis {
  name: string;
  force: Force;
  size: number;
  score: number;
  //todo: Add array of methods
}
