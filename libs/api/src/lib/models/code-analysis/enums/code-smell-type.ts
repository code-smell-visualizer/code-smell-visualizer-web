export enum CodeSmellType {
  ClassBased = 'CLASS_BASED',
  MethodBased = 'METHOD_BASED',
  Other = 'OTHER'
}
