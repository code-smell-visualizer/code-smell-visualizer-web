import {ClassAnalysis} from '@code-visualiser-web/api';

export class CodeAnalysisResponse {
  classes: Array<ClassAnalysis>
}
