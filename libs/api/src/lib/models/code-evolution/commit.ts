export interface Commit {
  id: number;
  author: string;
  branch: string;
  hash: string;
  message: string;
  name: string;
  time: Date;
  version: number;
}
