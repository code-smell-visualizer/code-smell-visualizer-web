import { Component } from '@angular/core';

@Component({
  selector: 'code-visualiser-web-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'code-visualiser';
  isIframe = false;
}
