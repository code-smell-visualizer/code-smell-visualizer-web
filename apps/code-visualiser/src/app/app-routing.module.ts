import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {D3Component} from './analysis/d3.component';
import {D3AdminComponent} from './analysis-admin/d3-admin.component';
import {AppLoadComponent} from './app-load/app-load.component';

const routes: Routes = [
  { path: '',
    component: D3Component
  },
  { path: 'upload',
    component: AppLoadComponent
  },
  { path: 'parameters',
    component:  D3AdminComponent
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
