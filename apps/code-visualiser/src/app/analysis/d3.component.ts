import {Component} from '@angular/core';
import {AnalysisFacade} from '@code-visualiser-web/code-analysis';

@Component({
  selector: 'code-visualiser-web-d3',
  templateUrl: './d3.component.html',
  styleUrls: ['./d3.component.scss']
})
export class D3Component {

  constructor(private analysisFacade: AnalysisFacade) {
  }

  codeAnalysis$ = this.analysisFacade.codeAnalysis$;
  colorRange$ = this.analysisFacade.colorRange$;
  isLoading$ = this.analysisFacade.isLoading$;
}
