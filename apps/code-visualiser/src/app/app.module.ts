import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { D3Component } from './analysis/d3.component';
import {D3Module} from '@code-visualiser-web/d3';
import {AppRoutingModule} from './app-routing.module';
import {CodeAnalysisModule} from '@code-visualiser-web/code-analysis';
import {StoreModule} from '@ngrx/store';
import {NxModule} from '@nrwl/angular';
import {EffectsModule} from '@ngrx/effects';
import {HttpClientModule} from '@angular/common/http';
import { D3AdminComponent } from './analysis-admin/d3-admin.component';
import { AppLoadComponent } from './app-load/app-load.component';
import {AppLoadModule} from '@code-visualiser-web/app-load';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [AppComponent, D3Component, D3AdminComponent, AppLoadComponent],
  imports: [
    AppLoadModule,
    BrowserModule,
    D3Module,
    AppRoutingModule,
    CodeAnalysisModule,
    HttpClientModule,
    NxModule.forRoot(),
    StoreModule.forRoot( {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
