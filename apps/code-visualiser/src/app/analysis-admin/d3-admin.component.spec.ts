import { ComponentFixture, TestBed } from '@angular/core/testing';

import { D3AdminComponent } from './d3-admin.component';

describe('D3ParametersComponent', () => {
  let component: D3AdminComponent;
  let fixture: ComponentFixture<D3AdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ D3AdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(D3AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
